package com.piyush.tic_tac_tae;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static Button b00,b01,b02,b10,b11,b12,b20,b21,b22;
    static int i1,i2,i3,i4,i5,i6,i7,i8,i9;
    static int j1,j2,j3,j4,j5,j6,j7,j8,j9;
    static TextView res;
    boolean player1=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b00=(Button)findViewById(R.id.btn00);
        b01=(Button)findViewById(R.id.btn01);
        b02=(Button)findViewById(R.id.btn02);
        b10=(Button)findViewById(R.id.btn10);
        b11=(Button)findViewById(R.id.btn11);
        b12=(Button)findViewById(R.id.btn12);
        b20=(Button)findViewById(R.id.btn20);
        b21=(Button)findViewById(R.id.btn21);
        b22=(Button)findViewById(R.id.btn22);
        res=(TextView)findViewById(R.id.result);

           b00.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View view) {

                    if (player1==true) {
                        b00.setText("X");
                        i1=10;
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        b00.setOnClickListener(null);
                    } else {
                        b00.setText("O");
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        player1=true;
                        j1=1;
                        b00.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b01.setText("X");
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        player1=false;
                        i2=10;
                        b01.setOnClickListener(null);
                    } else {
                        b01.setText("O");
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        player1 = true;
                        j2=1;
                        b01.setOnClickListener(null);
                    }
                    MainActivity.check();
                    }
            });
            b02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        b02.setText("X");
                        player1=false;
                        i3=10;
                        b02.setOnClickListener(null);

                    } else {
                        b02.setText("O");
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        player1 = true;
                        j3=1;
                        b02.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b10.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i4=10;
                        b10.setOnClickListener(null);
                    } else {
                        b10.setText("O");
                        player1 = true;
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        j4=1;
                        b10.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b11.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i5=10;
                        b11.setOnClickListener(null);

                    } else {
                        b11.setText("O");
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        player1 = true;
                        j5=1;
                        b11.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b12.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i6=10;
                        b12.setOnClickListener(null);
                    } else {
                        b12.setText("O");
                        player1 = true;
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        j6=1;
                        b12.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b20.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b20.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i7=10;
                        b20.setOnClickListener(null);
                    } else {
                        b20.setText("O");
                        player1 = true;
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        j7=1;
                        b20.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b21.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b21.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i8=10;
                        b21.setOnClickListener(null);
                    } else {
                        b21.setText("O");
                        player1 = true;
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        j8=1;
                        b21.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
            b22.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (player1==true) {
                        b22.setText("X");
                        player1=false;
                        Toast.makeText(MainActivity.this,"Player: 1",Toast.LENGTH_SHORT).show();
                        i9=10;
                        b22.setOnClickListener(null);
                    } else {
                        b22.setText("O");
                        player1 = true;
                        Toast.makeText(MainActivity.this,"Player: 2",Toast.LENGTH_SHORT).show();
                        j9=1;
                        b22.setOnClickListener(null);
                    }
                    MainActivity.check();
                }
            });
    }
    public static void check()
    {
        if(i1==10 && i2==10 && i3==10 )
        {
            res.setText("Player: 1 won");
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(i1==10 && i4==10  && i7==10)
        {
            res.setText("Player: 1 won");
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(i4==10 && i5==10 &&i6==10)
        {
            res.setText("Player: 1 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(i7==10 && i8==10 &&i9==10)
        {
            res.setText("Player: 1 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
        }
        else if(i2==10 && i5==10 &&i8==10)
        {
            res.setText("Player: 1 won");
            b00.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(i3==10 && i6==10 &&i9==10)
        {
            res.setText("Player: 1 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
        }
        else if(i1==10 && i5==10 && i9==10)
        {
            res.setText("Player: 1 won");
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
        }
        else if(i3==10 && i5==10 &&i7==10)
        {
            res.setText("Player: 1 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(j1==1 && j2==1 && j3==1 )
        {
            res.setText("Player: 2 won");
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(j1==1 && j4==1  && j7==1)
        {
            res.setText("Player: 2 won");
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(j4==1 && j5==1 && j6==1)
        {
            res.setText("Player: 2 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(j7==1 && j8==1 && j9==1)
        {
            res.setText("Player: 2 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
        }
        else if(j2==1 && j5==1 && j8==1)
        {
            res.setText("Player: 2 won");
            b00.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else if(j3==1 && j6==1 && j9==1)
        {
            res.setText("Player: 2 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b11.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
        }
        else if(j1==1 && j5==1 && j9==1)
        {
            res.setText("Player: 2 won");
            b01.setVisibility(View.INVISIBLE);
            b02.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b20.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
        }
        else if(j3==1 && j5==1 && j7==1)
        {
            res.setText("Player: 2 won");
            b00.setVisibility(View.INVISIBLE);
            b01.setVisibility(View.INVISIBLE);
            b10.setVisibility(View.INVISIBLE);
            b12.setVisibility(View.INVISIBLE);
            b21.setVisibility(View.INVISIBLE);
            b22.setVisibility(View.INVISIBLE);
        }
        else
        {
            res.setText("");
        }
    }


    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Really Exit")
                .setMessage("Are You Sure?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("Cancel",null);
        AlertDialog alert=builder.create();
        alert.show();
    }


}
